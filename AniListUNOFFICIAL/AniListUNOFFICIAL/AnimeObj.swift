//
//  Anime.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/9/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit
//
//Full Json objet to decode
struct AnimeDetail: Decodable{
    let data            : AnimeData
}

//
//Full Json objet to decode
struct Anime: Decodable{
    let data            : [AnimeData]
    let links           : Links
}

//
//Full Json links for appliying paying
struct Links: Decodable {
    let first           : String?
    let prev            : String?
    let next            : String?
    let last            : String?
}

//
//Struct for anime data [animeData] for tables AnimeData() for details views
struct AnimeData : Decodable{
    let id              : String?
    let type            : String?
    let attributes      : AnimeAttributes
    
    init() {
        self.id = nil
        self.type = nil
        self.attributes = AnimeAttributes()
    }
}

//
//Struct for anime main attributes to read
struct AnimeAttributes : Decodable{
    let posterImage     : PosterImage
    let canonicalTitle  : String?
    let synopsis        : String?
    let startDate       : String?
    let youtubeVideoId  : String?
    let userCount       : Int?
    let favoritesCount  : Int?
    let averageRating   : String?
    
    init() {
        self.posterImage    = PosterImage()
        self.canonicalTitle = nil
        self.synopsis       = nil
        self.startDate      = nil
        self.youtubeVideoId = nil
        self.userCount      = nil
        self.favoritesCount = nil
        self.averageRating  = nil
    }
}

//
//Struct for anime image variations
struct PosterImage : Decodable{
    let tiny            : String?
    let small           : String?
    let medium          : String?
    let large           : String?
    let original        : String?
    
    init() {
        self.tiny     = nil
        self.small    = nil
        self.medium   = nil
        self.large    = nil
        self.original = nil
    }
}
