//
//  AnimeSearchBar.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/16/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit

class AnimeSearchBar: NSObject, UISearchBarDelegate {
    let searchBar = UISearchBar()
    @IBOutlet weak var searchTableViewController: AnimeSearchTableViewController!
    @IBOutlet weak var animeSearchTableViewFetcher: AnimeSearchTablewViewFetcher!
    
    //
    //Adding Search bard dinamic
    func setUpSearchBar() {
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Buscar"
        searchBar.delegate = self
        
        searchTableViewController.navigationItem.titleView = searchBar
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        let text = searchBar.text
        animeSearchTableViewFetcher.fetchAnime(searchString: text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
