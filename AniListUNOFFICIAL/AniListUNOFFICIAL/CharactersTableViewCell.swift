//
//  CharactersTableViewCell.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/4/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit

class CharactersTableViewCell: UITableViewCell {
    //Properties
    //Anime Character
    @IBOutlet var imgAniCharacter: UIImageView!
    @IBOutlet var lblAniCharacterName: UILabel!
    @IBOutlet var lblAniCharacterType: UILabel!
    
    //Voice Character
    @IBOutlet var imgVoiceCharacter: UIImageView!
    @IBOutlet var lblVoiceCharacterName: UILabel!
    @IBOutlet var lblVoiceCharacterType: UILabel!
    
    //Content View Container
    @IBOutlet var cellViewItemContainer: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //
        //adding shadow to the cell container view
        cellViewItemContainer.layer.shadowColor = UIColor(red: 0/115.0, green: 0/115.0, blue: 0/115.0, alpha: 0.5).cgColor
        cellViewItemContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        cellViewItemContainer.layer.shadowRadius  = 5
        cellViewItemContainer.layer.shadowOpacity = 0.5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
