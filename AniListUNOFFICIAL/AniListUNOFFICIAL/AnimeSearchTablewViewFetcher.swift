//
//  AnimeSearchTablewViewFetcher.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/13/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit
import Alamofire

class AnimeSearchTablewViewFetcher: NSObject {
    //
    //Properties
    @IBOutlet weak var animeSearchTableViewController: AnimeSearchTableViewController!
    @IBOutlet weak var animeSearchTableViewStore: AnimeSearchTableViewStore!
    
    //
    //SETTING DATA
    func fetchAnime(searchString: String){
        animeSearchTableViewController.startLoadingView()
        let url = kitsuUrls.gets.animeSearch + searchString
        
        let task = Alamofire.request(url).responseJSON{
            response in
            
            if let data = response.data, let _ = String(data: data, encoding: .utf8) {
                do{
                    //Decoding full json file to an struct
                    let animeObj = try JSONDecoder().decode(Anime.self, from: data)
                    //populing array from obj.data
                    self.animeSearchTableViewStore.animeArray = animeObj.data
                }catch let e{
                    print("Error \(e.localizedDescription)")
                }
            }
            else{
                print("Erron fetching data")
            }
            
            self.animeSearchTableViewController.animeSearchTableView.reloadData()
            self.animeSearchTableViewController.stopLoadingView()
        }
        task.resume()
    }
}
