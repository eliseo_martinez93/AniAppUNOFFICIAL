//
//  DetailViewController.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/4/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit
import SVProgressHUD

class AnimeDetailViewController: UIViewController {
    
    @IBOutlet var animeDetailStore: AnimeDetailStore!
    
    //
    //parsing url data
    var detailAnimeId = String()
    
    //
    //Loading indicator
    var indicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //
    //Properties
    //SegmentControl for hide and show viewa
    @IBOutlet var scTab: UISegmentedControl!
    @IBOutlet var charactersTableVIew: UITableView!
    
    //View emulating tabs
    @IBOutlet var characterView: UIView!
    @IBOutlet var detailView: UIView!
    
    //details view outlets
    @IBOutlet var lblAniName: UILabel!
    @IBOutlet var imgAnime: UIImageView!
    @IBOutlet var lblAniType: UILabel!
    @IBOutlet var lblAniRealeaseDate: UILabel!
    
    //statistics
    @IBOutlet var lblAverageScore: UILabel!
    @IBOutlet var lblMeanScore: UILabel!
    @IBOutlet var lblPopularity: UILabel!
    
    //Others details view outlets
    @IBOutlet var lblAniDescription: UILabel!
    @IBOutlet var imgNoVideoAvailable: UIImageView!
    @IBOutlet var wvYouTubeVideo: UIWebView!
    
    @IBOutlet var animeDetailViewFetcher: AnimeDetailViewFetcher!
    //END
    //Properties
    
    func startLoadingView(){
        SVProgressHUD.show(withStatus: "Cargando")
    }
    
    func stopLoadingView() {
        SVProgressHUD.dismiss()
    }
    
    //END
    //LOADING DATA FUNCTIONS
    
    
    //
    //Common Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Get details data
        animeDetailViewFetcher.fetchAnimeDetail()
        animeDetailStore.characterArray = AnimeDataDemoHelper.LoadinDemoAnimeCharacterList()
        
        //styling details view
        detailView.layer.shadowColor    = UIColor(red: 0/115.0, green: 0/115.0, blue: 0/115.0, alpha: 0.5).cgColor
        detailView.layer.shadowOffset   = CGSize(width: 0, height: 0)
        detailView.layer.shadowRadius   = 5
        detailView.layer.shadowOpacity  = 0.5
    }
    
    func setUpView() {
        //populating detail segment control data
        //Data to show
        self.lblAniName.text            = animeDetailStore.animeDetail.attributes.canonicalTitle
        self.lblAniDescription.text     = animeDetailStore.animeDetail.attributes.synopsis
        self.lblAniType.text            = animeDetailStore.animeDetail.type
        self.lblAniRealeaseDate.text    = animeDetailStore.animeDetail.attributes.startDate
        
        let youtubeVideoId          = animeDetailStore.animeDetail.attributes.youtubeVideoId
        let averageScore            = animeDetailStore.animeDetail.attributes.userCount
        let meanScore               = animeDetailStore.animeDetail.attributes.favoritesCount
        
        self.lblAverageScore.text   = "\(averageScore ?? 00)"
        self.lblMeanScore.text      = "\(meanScore ?? 00)"
        
        self.lblPopularity.text     = animeDetailStore.animeDetail.attributes.averageRating
        let animeImage              = animeDetailStore.animeDetail.attributes.posterImage.medium
        
        if let url = animeImage {
            self.imgAnime.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "posterImage.png"))
        }
        
        self.wvYouTubeVideo.loadHTMLString("<iframe width=\"\(self.wvYouTubeVideo.frame.width)\" height=\"\(self.wvYouTubeVideo.frame.height)\" src=\"https://www.youtube.com/embed/\(youtubeVideoId ?? "0000")\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>", baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeView(_ sender: Any) {
        let tabValue = scTab.selectedSegmentIndex
        
        if tabValue == 0 {
            detailView.isHidden = false
            characterView.isHidden = true
        }else{
            detailView.isHidden = true
            characterView.isHidden = false
        }
    }
}

