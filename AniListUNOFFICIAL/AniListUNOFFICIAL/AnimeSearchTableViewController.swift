//
//  AnimeSearchTableViewController.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/13/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit
import SVProgressHUD

class AnimeSearchTableViewController: UITableViewController {
    //
    //Properties
    @IBOutlet var animeSearchTableView: UITableView!
    @IBOutlet var animeSearchTableViewStore: AnimeSearchTableViewStore!
    
    @IBOutlet var animeSearchBar: AnimeSearchBar!
    var indicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func startLoadingView(){
        SVProgressHUD.show(withStatus: "Cargando")
    }
    
    func stopLoadingView() {
        SVProgressHUD.dismiss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.animeSearchBar.setUpSearchBar()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == ApplicationStringsHelper.segueNames.searchToDetail
        {
            let detailV = segue.destination as! AnimeDetailViewController
            
            let cellIndex = tableView.indexPathForSelectedRow?.row
            
            let animeId = animeSearchTableViewStore.animeArray[cellIndex!].id
            detailV.detailAnimeId = animeId!
        }
    }
}
