//
//  AnimeDetailViewFetcher.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/16/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class AnimeDetailViewFetcher: NSObject {
    //
    //Properties
    @IBOutlet weak var animeDetailViewController: AnimeDetailViewController!
    @IBOutlet weak var animeDetailStore: AnimeDetailStore!
    
    
    //
    //SETTING DATA
    func fetchAnimeDetail(){
        animeDetailViewController.startLoadingView()
        let url = kitsuUrls.gets.animeDetail + "\(self.animeDetailViewController.detailAnimeId)"
        
        let task = Alamofire.request(url).responseJSON{
            response in
            
            if let data = response.data, let _ = String(data: data, encoding: .utf8) {
                do{
                    //Decoding full json file to an struct
                    let animeObj = try JSONDecoder().decode(AnimeDetail.self, from: data)
                    
                    self.animeDetailStore.animeDetail = animeObj.data
                    self.animeDetailViewController.setUpView()
                    
                }catch let e{
                    print("Error \(e.localizedDescription)")
                }
            }
            else{
                print("Erron fetching data")
            }
            self.animeDetailViewController.stopLoadingView()
        }
        task.resume()
    }
}
