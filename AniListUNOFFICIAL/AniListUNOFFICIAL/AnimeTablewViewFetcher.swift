//
//  AnimeTablewViewFetcher.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/13/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit
import Alamofire

class AnimeTablewViewFetcher: NSObject {
    //
    //Properties
    @IBOutlet weak var animeTableViewController: AnimeTableViewController!
    @IBOutlet weak var animeTableViewStore: AnimeTableViewStore!
    
    //
    //SETTING DATA
    func fetchAnime(){
        animeTableViewController.startLoadingView()
        let url = kitsuUrls.gets.animeList
        
        let task = Alamofire.request(url).responseJSON{
            response in
            
            if let data = response.data, let _ = String(data: data, encoding: .utf8) {
                do{
                    //Decoding full json file to an struct
                    let animeObj = try JSONDecoder().decode(Anime.self, from: data)
                    //populing array from obj.data
                    self.animeTableViewStore.animeArray = animeObj.data
                }catch let e{
                    print("Error \(e.localizedDescription)")
                }
            }
            else{
                print("Erron fetching data")
            }
            
            self.animeTableViewController.animeTableView.reloadData()
            self.animeTableViewController.stopLoadingView()
        }
        task.resume()
    }
}
