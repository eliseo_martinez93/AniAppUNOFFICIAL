//
//  AnimeTableViewDataSource.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/13/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit
import SDWebImage

class AnimeTableViewDataSource: NSObject, UITableViewDataSource {
    //
    //Properties
    @IBOutlet weak var animeTableViewStore: AnimeTableViewStore!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return animeTableViewStore.animeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ApplicationStringsHelper.cells.animeTableViewCellName) as? AnimeTableViewCell else{
            return UITableViewCell()
        }
        let animeArray = animeTableViewStore.animeArray
        
        cell.lblAnimeName.text          = animeArray[indexPath.row].attributes.canonicalTitle
        cell.lblAnimeDescription.text   = animeArray[indexPath.row].attributes.synopsis
        cell.lblAnimeSerieType.text     = animeArray[indexPath.row].type
        
        let urlImage = animeArray[indexPath.row].attributes.posterImage.medium
        
        if let url = urlImage {
            cell.imgAnime.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "posterImage.png"))
        }

        return cell
    }
}
