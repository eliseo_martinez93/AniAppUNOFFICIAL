//
//  AnimeRestClient.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/12/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import Foundation

struct kitsuUrls {
    struct gets {
        static let animeList : String = "https://kitsu.io/api/edge/anime?page[limit]=20&page[offset]=0"
        static let animeSearch : String = "https://kitsu.io/api/edge/anime?filter[text]="
        static let animeDetail : String = "https://kitsu.io/api/edge/anime/"
    }
}
