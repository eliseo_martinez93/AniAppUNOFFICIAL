//
//  AnimeCharacter.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/9/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit

class AnimeCharacter: NSObject {
    let aniCharacterName    : String
    let aniCharacterType    : String
    let aniCharacterImage   : String
    let voiceCharacterName  : String
    let voiceCharacterType  : String
    let voiceCharacterImage : String
    
    init(aniCharacterName: String, aniCharacterType: String, aniCharacterImage: String ,voiceCharacterName: String, voiceCharacterType: String, voiceCharacterImage: String) {
        self.aniCharacterName       = aniCharacterName
        self.aniCharacterType       = aniCharacterType
        self.aniCharacterImage      = aniCharacterImage
        self.voiceCharacterName     = voiceCharacterName
        self.voiceCharacterType     = voiceCharacterType
        self.voiceCharacterImage    = voiceCharacterImage
    }
}
