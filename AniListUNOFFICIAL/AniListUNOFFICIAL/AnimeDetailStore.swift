//
//  AnimeDetailStore.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/16/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit

class AnimeDetailStore: NSObject {
    var animeDetail  = AnimeData()
    var characterArray = [AnimeCharacter]()
}
