//
//  characterTableViewDataSource.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/16/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit

class CharacterTableViewDataSource: NSObject, UITableViewDataSource {
    
    
    @IBOutlet weak var animeDetailStore: AnimeDetailStore!
    @IBOutlet weak var detailViewController: AnimeDetailViewController!
    
    //
    //table View methos
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.animeDetailStore.characterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ApplicationStringsHelper.cells.characterTableViewCellName) as? CharactersTableViewCell else{
            return UITableViewCell()
        }
        
        //Setting Ani Character Data
        cell.lblAniCharacterName.text   = self.animeDetailStore.characterArray[indexPath.row].aniCharacterName
        cell.lblAniCharacterType.text   = self.animeDetailStore.characterArray[indexPath.row].aniCharacterType
        cell.imgAniCharacter.image      = UIImage(named: self.animeDetailStore.characterArray[indexPath.row].aniCharacterImage)
        
        //Setting Voice Character Data
        cell.lblVoiceCharacterName.text   = self.animeDetailStore.characterArray[indexPath.row].voiceCharacterName
        cell.lblVoiceCharacterType.text   = self.animeDetailStore.characterArray[indexPath.row].voiceCharacterType
        cell.imgVoiceCharacter.image      = UIImage(named: self.animeDetailStore.characterArray[indexPath.row].voiceCharacterImage)
        
        return cell
    }
    
}
