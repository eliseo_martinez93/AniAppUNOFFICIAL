//
//  ApplicationCellNames.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/13/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import Foundation

struct ApplicationStringsHelper {
    struct cells {
        static let animeTableViewCellName : String = "custom_AnyTableViewCell"
        static let characterTableViewCellName : String = "characters_customTableCell"
    }
    struct segueNames {
        static let animeToDetail: String = "mainToDetail"
        static let searchToDetail: String = "searchToDetail"
    }
}
