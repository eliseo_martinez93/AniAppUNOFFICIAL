//
//  AnimeViewController.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/4/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit
import SVProgressHUD

class AnimeTableViewController: UITableViewController {
    //
    //PROPERTIES
    @IBOutlet var animeTableView: UITableView!
    @IBOutlet var animeTableViewFetcher: AnimeTablewViewFetcher!
    @IBOutlet var animeTableViewStore: AnimeTableViewStore!
    
    var indicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func startLoadingView(){
        SVProgressHUD.show(withStatus: "Cargando")
    }
    
    func stopLoadingView() {
        SVProgressHUD.dismiss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setUpAnime()
        animeTableViewFetcher.fetchAnime()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == ApplicationStringsHelper.segueNames.animeToDetail
        {
            let detailV = segue.destination as! AnimeDetailViewController
            
            let cellIndex = tableView.indexPathForSelectedRow?.row
            
            let animeId = animeTableViewStore.animeArray[cellIndex!].id
            detailV.detailAnimeId = animeId!
        }
    }
 

}
