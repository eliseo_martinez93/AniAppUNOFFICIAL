//
//  AnimeRestClientHelper.swift
//  AniListUNOFFICIAL
//
//  Created by Eliseo_Martinez on 7/9/18.
//  Copyright © 2018 doodle-software. All rights reserved.
//

import UIKit

class AnimeDataDemoHelper: NSObject {
    //
    //  Mockup Data for Anime/Chharacters
    //  Array used in detaails characters tablew view controller for mockup data
    //
    public static func LoadinDemoAnimeCharacterList() -> [AnimeCharacter] {
        //
        //Array data demo
        var animeCharacterArray = [AnimeCharacter]()
        
        animeCharacterArray.append(AnimeCharacter(aniCharacterName: "Rinne Ohara", aniCharacterType: "Main", aniCharacterImage: "cell_character_image_demo1", voiceCharacterName: "Rinne Ohara", voiceCharacterType: "Main", voiceCharacterImage: "cell_character_image_demo1_1"))
        animeCharacterArray.append(AnimeCharacter(aniCharacterName: "Karen Kurutsu", aniCharacterType: "Main", aniCharacterImage: "cell_character_image_demo2", voiceCharacterName: "Kana Asumi", voiceCharacterType: "Main", voiceCharacterImage: "cell_character_image_demo2_1"))
        animeCharacterArray.append(AnimeCharacter(aniCharacterName: "Rinne Ohara", aniCharacterType: "Main", aniCharacterImage: "cell_character_image_demo1", voiceCharacterName: "Rinne Ohara", voiceCharacterType: "Main", voiceCharacterImage: "cell_character_image_demo1_1"))
        animeCharacterArray.append(AnimeCharacter(aniCharacterName: "Karen Kurutsu", aniCharacterType: "Main", aniCharacterImage: "cell_character_image_demo2", voiceCharacterName: "Kana Asumi", voiceCharacterType: "Main", voiceCharacterImage: "cell_character_image_demo2_1"))
        animeCharacterArray.append(AnimeCharacter(aniCharacterName: "Rinne Ohara", aniCharacterType: "Main", aniCharacterImage: "cell_character_image_demo1", voiceCharacterName: "Rinne Ohara", voiceCharacterType: "Main", voiceCharacterImage: "cell_character_image_demo1_1"))
        animeCharacterArray.append(AnimeCharacter(aniCharacterName: "Karen Kurutsu", aniCharacterType: "Main", aniCharacterImage: "cell_character_image_demo2", voiceCharacterName: "Kana Asumi", voiceCharacterType: "Main", voiceCharacterImage: "cell_character_image_demo2_1"))
        
        return animeCharacterArray
    }
}
